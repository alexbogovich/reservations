package com.alexbogovich.reservations.buisness.service;

import com.alexbogovich.reservations.buisness.domain.RoomReservation;
import com.alexbogovich.reservations.data.entity.Guest;
import com.alexbogovich.reservations.data.entity.Reservation;
import com.alexbogovich.reservations.data.entity.Room;
import com.alexbogovich.reservations.data.repository.GuestRepository;
import com.alexbogovich.reservations.data.repository.ReservationRepository;
import com.alexbogovich.reservations.data.repository.RoomRepository;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReservationService {
    private RoomRepository roomRepository;
    private GuestRepository guestRepository;
    private ReservationRepository reservationRepository;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public ReservationService(RoomRepository roomRepository, GuestRepository guestRepository, ReservationRepository reservationRepository) {
        this.roomRepository = roomRepository;
        this.guestRepository = guestRepository;
        this.reservationRepository = reservationRepository;
    }

    public List<RoomReservation> getRoomReservationForDate(String dateString) {
        Date date = this.createDateFromDateString(dateString);
        Iterable<Room> rooms = this.roomRepository.findAll();
        Map<Long, RoomReservation> roomReservationMap = new HashMap<>();
        rooms.forEach(room -> {
            RoomReservation roomReservation = new RoomReservation();
            roomReservation.setRoomId(room.getId());
            roomReservation.setRoomName(room.getName());
            roomReservation.setRoomNumber(room.getNumber());
            roomReservationMap.put(room.getId(), roomReservation);
        });

        Iterable<Reservation> reservations = this.reservationRepository
                .findByDate(new java.sql.Date(date.getTime()));

        List<RoomReservation> roomReservations = new ArrayList<>();

        if (null != reservations) {
            reservations.forEach(reservation -> {
                Guest guest = this.guestRepository.findOne(reservation
                        .getGuestId());
                if (null != guest) {
                    RoomReservation roomReservation = roomReservationMap.
                            get(reservation.getId());
                    roomReservation.setDate(date);
                    roomReservation.setFirstName(guest.getFirstName());
                    roomReservation.setLastName(guest.getLastName());
                    roomReservation.setGuestId(guest.getId());
                }
            });


            for (Long roomId : roomReservationMap.keySet()) {
                roomReservations.add(roomReservationMap.get(roomId));
            }

        }

        return roomReservations;
    }

    private Date createDateFromDateString(String dateString){
        Date date = null;
        if(null!=dateString) {
            try {
                date = DATE_FORMAT.parse(dateString);
            }catch(ParseException pe){
                date = new Date();
            }
        }else{
            date = new Date();
        }
        return date;
    }
}
